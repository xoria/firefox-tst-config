# Tree Style Tab Config

1. Copy the chrome/ directory into your firefox profile directory (e.g. ~/.mozilla/firefox/*/)
2. Adjust about:config properties as specified within about.config file
3. Adjust TST extension settings
    * Load the configs-treestyletab... file, or apply at least the following settings
    * Theme: Proton
    * When a parent tab is Closed or Moved:
        * Recommended Preset using Consistent Tab Behaviors controlled under TST
        * Promote the First Child on the root level, otherwise Promote All Children
    * Extra style rules: Load treestyletab.css
